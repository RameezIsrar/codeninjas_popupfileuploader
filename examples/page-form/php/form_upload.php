<?php
    include('../../../src/class.fileuploader.php');
    /**
     *
     */
    class FileUpload
    {

      // function __construct(argument)
      // {
      //   # code...
      // }

        function save_file(){
        // initialize FileUploader
          $FileUploader = new FileUploader('files', array(
              'limit' => 1,
              'maxSize' => null,
          'fileMaxSize' => null,
              'extensions' => ['jpg', 'jpeg', 'png', 'gif'],
              'required' => false,
              'uploadDir' => '../uploads/',
              'title' => 'my_avatar',
          'replace' => true,
              'listInput' => true,
              'files' => null
          ));

        // call to upload the files
          $data = $FileUploader->upload();
        if($data['hasWarnings']) {
              $warnings = $data['warnings'];

            echo '<pre>';
              print_r($warnings);
          echo '</pre>';
          }

        if($data['isSuccess'] && count($data['files']) > 0) {
          $file = $data['files'][0]['file'];
          $filename = $data['files'][0]['name'];
          // echo our form data
          echo '<h1>Your form data:</h1>';
          echo 'Avatar:<br> <img src="' . $file . '" style="max-width: 400px; max-height: 400px">';
        }

      } // save function block ends

    }

    $obj = new FileUpload ;
    $obj ->save_file();
